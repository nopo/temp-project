﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private Rigidbody2D rigidbody;
	public float speed = 8;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		rigidbody.AddForce (new Vector2 (speed, 0));
	
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "LeftBoundary") {
			TurnRight ();
		} else if (other.tag == "RightBoundary") {
			TurnLeft ();
		}
	}

	private void TurnLeft(){
		rigidbody.velocity = Vector3.zero;
		speed = Mathf.Abs (speed) * -1;
	}

	private void TurnRight(){
		rigidbody.velocity = Vector3.zero;
		speed = Mathf.Abs (speed);
	}

}
