﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	public float speed;
	public float jumpForce;
	private int health = 5;
	private bool isGrounded = false;
	private Rigidbody2D rigidbody;
	private Animator animator;


	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Attack ();
		}


	}

	//gets called for every fixed framerate frame - use for objects with rigidbodies
	void FixedUpdate(){
		//declaring a variable
		float moveHorizontal;
		Vector3 groundcheck = transform.position;

		//initializing a variable
		moveHorizontal = Input.GetAxis("Horizontal");

		//if moveHorizontal is not 0, set playerWalking to true
		if(moveHorizontal != 0){
			animator.SetBool ("playerWalking", true);
		}else{
			animator.SetBool ("playerWalking", false);
		}
		//otherwise, set playerWalking to false.
		rigidbody.AddForce (new Vector2 (moveHorizontal * speed, 0));

		//check if we are grounded
		// groundcheck.y = groundcheck.y -1;
		groundcheck.y -= 1;
		isGrounded = Physics2D.Linecast (transform.position, groundcheck, 1 << LayerMask.NameToLayer ("Ground"));

		//IF player hits jump button, add force to y axis
		if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)){
			Jump ();
		}
	}

	private void Jump(){
		if (isGrounded) {
			rigidbody.AddForce (new Vector2 (0, jumpForce));
		}
	}

	private void Attack(){
		animator.SetTrigger ("playerAttack");
	}

	private void OnTriggerEnter2D(Collider2D other){
		//if hat then reload current level
		if (other.tag == "Hat") {
			GameController.Instance.NewLevel (1);
		} else if (other.tag == "Bomb") {
			//else if bomb then make player disappear
			gameObject.SetActive (false);
		} else if (other.tag == "GameOverBoundary") {
			GameController.Instance.GameOver ();
		}
	}
}
