﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	private Text levelText;
	private GameObject startScreen;
	private GameObject gameOverScreen;

	void Awake(){
		if (Instance != null && Instance != this) {
			DestroyImmediate (gameObject);
			return;
		}


		Instance = this;
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		levelText = GameObject.Find ("Level Text").GetComponent<Text> ();
		startScreen = GameObject.Find ("StartScreen");
		gameOverScreen = GameObject.Find ("GameOverScreen");
		gameOverScreen.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void NewLevel(int level){
		levelText.text = "Level " + (level + 1);
		SceneManager.LoadScene (level);
	}

	public void StartGame(){
		startScreen.SetActive (false);
	}

	public void RestartGame(){
		//Reset our player to level 1
		NewLevel(0);

		//Reset any global variables - if you have global variables that have changed

		//Deactivate my game over screen
		gameOverScreen.SetActive(false);


		//Activate my start screen - maybe?
		startScreen.SetActive(true);
	}

	public void GameOver(){
		gameOverScreen.SetActive (true);
	}
}
